import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  console.log('fighter', fighter)
  fighterElement.append(createFighterImage(fighter));
  // const [source, name, health, ...rest] = fighter;
  // const attributes = {
  //   src: `${source}`,
  //   title: name
  // };
  // const elimg = createElement({
  //   tagName: 'img',
  //   className: 'fighter___fighter-image',
  //   attributes
  // });

  
  const elhealth = document.createElement('p');
  elhealth.innerHTML = `health: ${fighter.health}`;

  fighterElement.append(elhealth);
  
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
