import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    if (firstFighter.health = 0) {
      resolve(secondFighter)
    };
    if (secondFighter.health = 0) {
      resolve(firstFighter)
    }
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  return attacker.attack > defender.defense ? getHitPower(attacker.attack)- getBlockPower(defender.defense) : 0;
  // return damage
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandom(1,2);
  const power = fighter.attacker * criticalHitChance;
  return power;
  // return hit power
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandom(1,2);
  const power = fighter.defense * dodgeChance;
  return power;
  // return block power
}


function getRandom (min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.random() * (max - min + 1) + min;
};
